#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    collatz_compute,
    collatz_cache,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    global collatz_cache
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # ----------
    # unit tests
    # ----------

    def test_eval_5(self):
        v = collatz_compute(1000000)
        self.assertEqual(v, 153)

    def test_eval_6(self):
        v = collatz_compute(987654)
        self.assertEqual(v, 184)

    def test_eval_7(self):
        v = collatz_cache[1000000]
        self.assertEqual(v, 153)

    def test_eval_8(self):
        v = collatz_cache[987654]
        self.assertEqual(v, 184)

    def test_eval_9(self):
        v = collatz_cache[493827]
        self.assertEqual(v, 183)

    def test_eval_10(self):
        v = collatz_compute(740741)
        self.assertEqual(v, 181)

    def test_eval_11(self):
        v = collatz_cache[740741]
        self.assertEqual(v, 181)

    def test_eval_12(self):
        v = collatz_cache[555556]
        self.assertEqual(v, 178)

    def test_eval_13(self):
        v = collatz_eval(1234, 1234)
        self.assertEqual(v, 133)

    def test_eval_14(self):
        v = collatz_eval(1, 1000000)
        self.assertEqual(v, 525)

    def test_eval_15(self):
        v = collatz_compute(2000000)
        self.assertEqual(v, 154)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
