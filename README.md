# CS373: Software Engineering Collatz Repo

* Name: Simon Pinochet

* EID: sp44454

* GitLab ID: simon.pinochet

* HackerRank ID: simon_pinochet

* Git SHA: 0a1591bef12015d5e5023a24d49e8a970761efa9

* GitLab Pipelines: https://gitlab.com/simon.pinochet/cs373-collatz/pipelines

* Estimated completion time: 2 hours

* Actual completion time: About 2 hours of coding, an hour to realize i could be higher than j, 5 hours preparing for submition (writing tests, figuring out black, pylint, pydoc, etc).

* Comments: For the next projects I'll try to set up docker. Installing all the tools was hard and even something that should've taken minutes took hours.
